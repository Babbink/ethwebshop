import CompanyRouter from "./company.routes.js";
import express from "express";
const app = express();

app.use("/", CompanyRouter);

export default app;
