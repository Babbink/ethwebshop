import express from "express";
const router = express.Router();

router.get("/", (req, res) => {
  res.send("Get all the products");
});

router.get("/:productID", (req, res) => {
  res.send(`Get the product, it's index is: ${req.params.productID}`);
});

router.post("/:productID/buy", (req, res) => {
  res.send(`You bought a product ${req.params.productID}`);
});

router.get("/admin", (req, res) => {
  res.send("This should be the admin panel");
});

router.get("/admin/products", (req, res) => {
  res.send("Show all the products in a table");
});

router.post("/admin/products/create", (req, res) => {
  res.send("Create a new product");
});

router.delete("/admin/products/:productID", (req, res) => {
  res.send(`Delete product with the index of: ${req.params.productID}`);
});

router.patch("/admin/products/:productID", (req, res) => {
  res.send(`Update product with the index of: ${req.params.productID}`);
});

export default router;
