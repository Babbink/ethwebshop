// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.7;

contract WebshopFactory {
    Webshop[] deployedWebshops;
    
    function createWebshop(string memory name, address payable owner) public {
        Webshop newWebshop = new Webshop(name, owner);
        deployedWebshops.push(newWebshop);
    }
    
    function getDeployedWebshops() public view returns(Webshop[] memory) {
        return deployedWebshops;
    }
}

contract Webshop {
    string public shopName;
    address payable public shopManager;
    
    constructor(string memory name, address payable admin) {
        shopName = name;
        shopManager = admin;
    }
    
     modifier restricted() {
        require(msg.sender == shopManager);
        _;
     }
    
    struct Product {
        string productName;
        uint weiPrice;
        uint stock;
        bool available;
        uint sold;
    }

    Product[] public products;

    function createProduct(string memory productName, uint price, uint stock, bool available) public  restricted {
        available = available || false;
        
        // If stock equals 0 and the product is available, there can be sold an unlimited amount of that product
        // If a product has a certain stock, it will automatic ally become unavailable if there is no more stock
        products.push(Product({
            productName: productName,
            weiPrice: price,
            stock: stock,
            available: available,
            sold: 0
        }));
    }
    
    function getProducts() public view returns (Product[] memory) {
        return products;
    }
    
    function deleteProduct(uint index) public restricted {
        delete products[index];
    }
    
    function updateProduct(uint index, string memory updatedProductName, uint updatedStock, uint updatedPrice, bool updatedAvailable) public restricted {
        products[index].productName = updatedProductName;
        products[index].stock = updatedStock;
        products[index].weiPrice = updatedPrice;
        products[index].available = updatedAvailable || products[index].available;
    }
    
    function updateProductName(uint index, string memory updatedProductName) public restricted {
        products[index].productName = updatedProductName;
    }
    
    function updateProductPrice(uint index, uint updatedPrice) public restricted {
        products[index].weiPrice = updatedPrice;
    }
    
    function updateProductStock(uint index, uint updatedStock) public restricted {
        products[index].stock = updatedStock;
    }
    
    function toggleAvailability(uint index) public restricted {
        products[index].available = !products[index].available;
    }
    
    function duplicateProduct(uint index) public restricted {
        products.push(Product({
            productName: products[index].productName,
            weiPrice: products[index].weiPrice,
            stock: products[index].stock,
            available: products[index].available,
            sold: 0
        }));
    }
    
    function buyProductNow(uint index) public payable {
        require(products[index].available == true);
        require(msg.value == products[index].weiPrice);
        shopManager.transfer(msg.value);
        if (products[index].stock > 0) {
            products[index].stock = products[index].stock - 1;
            products[index].sold = products[index].sold + 1;
            if (products[index].stock == 0) products[index].available = false;
        }
    }
}
