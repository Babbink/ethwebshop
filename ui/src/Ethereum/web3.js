const Web3 = require("web3");

let web3;

if (typeof window !== "undefined" && window.web3) {
  // We are in the browser and metamask is running.
  web3 = new Web3(window.web3.currentProvider);
} else {
  // We are on the server *OR* the user is not running metamask
  const provider = new Web3.providers.HttpProvider(
    "https://rinkeby.infura.io/v3/4dc56098b7e04f518ed4e6b802b8279a"
  );
  web3 = new Web3(provider);
}

export default web3;
