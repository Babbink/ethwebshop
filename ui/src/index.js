import React from "react";
import ReactDOM from "react-dom";
import Home from "./Home";

import 'semantic-ui-css/semantic.min.css'

ReactDOM.render(
  <React.StrictMode>
    <Home />
  </React.StrictMode>,
  document.getElementById("app")
);
