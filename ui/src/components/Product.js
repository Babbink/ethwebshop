import React from "react";
import axios from "axios";
import { Card, Button } from "semantic-ui-react";
import web3 from "../Ethereum/web3.js";
import Campaign from "./../Ethereum/Campaign.json";

const items = [
  {
    productName: "iPhone 11",
    stock: "11 left in stock",
    price: "50000000000000000",
  },
  {
    productName: "iPhone 12",
    stock: "12000 left in stock",
    price: "100000000000000000",
  },
  {
    productName: "iPhone 13",
    stock: "29000 left in stock",
    price: "200000000000000000",
  },
];

const buyProduct = async (item) => {
  const [account] = await web3.eth.getAccounts();
  alert(`Account: ${account}`);
  let address = prompt("Deliver Address: ");
  let email = prompt("Email: ");
  console.log(address, email);
  await web3.eth.sendTransaction({ from: account, to: "0x9FA4653799Aaf00df125C8cA5e8BBe327288B3aa", value: item.price }).then(async (trx) => {
    let transactionDone = await axios.get(`https://api.etherscan.io/api?module=transaction&action=gettxreceiptstatus&txhash=${trx.transactionHash}&apikey=1EP86GAXKWQRJJF76RDYESIXR7PIJZ6FK1`);
    if (transactionDone.data.result.status === "1") console.log("Dit is een echte transactie, geen testnetwerk transactie");

    // Als de transactie aangekomen is mag een mail verzonden worden dat een product gekocht en betaald is
  });
};

const contract = async () => {
  console.log(await new web3.eth.Contract(JSON.parse(Campaign.interface), "0x1352D389425C518B94099DF3E72e9520dB6b614F"));
};

const CardExampleGroupProps = () => {
  contract();
  return (
    <Card.Group>
      {items.map((item) => {
        return (
          <Card key={item.productName}>
            <Card.Content>
              <Card.Header>{item.productName}</Card.Header>
              <Card.Meta>{item.stock}</Card.Meta>
              <Card.Description>{web3.utils.fromWei(item.price, "ether")} ETH</Card.Description>
            </Card.Content>
            <Card.Content extra>
              <div className="ui two buttons">
                <Button inverted color="green" onClick={() => buyProduct(item)}>
                  Buy
                </Button>
                <Button color="white">More info</Button>
              </div>
            </Card.Content>
          </Card>
        );
      })}
    </Card.Group>
  );
};
<Card.Group items={items} />;

export default CardExampleGroupProps;
